part of 'screens.dart';

class HomeScreen extends ConsumerStatefulWidget {
  const HomeScreen({ Key? key }) : super(key: key);

  @override
  _HomeScreenState createState() => _HomeScreenState();
}

class _HomeScreenState extends ConsumerState<HomeScreen> {


  @override
  void initState() {
    super.initState();
    initApp();
    
  }

  void initApp() async {
    await ref.read(controllerWeather).getLocation();
    ref.read(controllerWeather).getWeather();
  }


  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.blue[200] ,
      appBar: AppBar(
        backgroundColor: Colors.transparent,
        elevation: 0,
        centerTitle: false,
        title: Row(
          children: [
            Image.asset("assets/icons/ic_location.png", width: 20, color: masterColor('secondary'),),
            const SizedBox(width: 8),
            Text(
              ref.watch(controllerWeather).currentCity ?? 'Getting Location..',
              style: GoogleFonts.montserrat(
                color: masterColor('secondary'),
                fontSize: 20,
                fontWeight: masterFontWeight('SB')
              ),
            ),
            const Spacer(),
            Image.asset("assets/icons/ic_menu.png", width: 20, color: masterColor('secondary'),),
          ],
        )
      ),
      body: SlidingUpPanel(
        minHeight: 240,
        borderRadius: BorderRadius.circular(20),
        body: Stack(
          children: [
            Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 24.0),
                  child: Text(
                    "Today, Jun 2 20:30",
                    style: GoogleFonts.montserrat(
                      color: masterColor('secondary'),
                      fontSize: 16,
                      fontWeight: masterFontWeight('M')
                    ),
                  ),
                ),
                const SizedBox(height: 20),
                Align(
                  alignment: Alignment.centerRight,
                  child: Padding(
                    padding: const EdgeInsets.symmetric(horizontal: 24.0),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.end,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text(
                          ref.watch(controllerWeather).dataWeather != null ? ref.watch(controllerWeather).dataWeather!.current!.tempC.toString() : '0',
                          style: GoogleFonts.montserrat(
                            color: masterColor('secondary'),
                            fontSize: 100,
                            fontWeight: masterFontWeight('SB')
                          ),
                        ),
                        Padding(
                          padding: const EdgeInsets.only(top: 10.0),
                          child: Text(
                            "°",
                            style: GoogleFonts.montserrat(
                              color: masterColor('secondary'),
                              fontSize: 40,
                              fontWeight: masterFontWeight('M')
                            ),
                          ),
                        )
                        
                      ],
                    )
                  ),
                ),
              ],
            ),
          ],
        ),
        panel: Padding(
          padding: const EdgeInsets.symmetric(vertical: 18),
          child: Column(
            children: [
              Container(
                margin: const EdgeInsets.only(bottom: 15),
                height: 4,
                width: 50,
                decoration: BoxDecoration(
                  color: masterColor('disabled'),
                ),
              ),
              Text(
                "Weather Today",
                style: GoogleFonts.montserrat(
                  color: masterColor('primary'),
                  fontSize: 16,
                  fontWeight: masterFontWeight('SB')
                ),
              ),
              const SizedBox(height: 30),
              ref.watch(controllerWeather).dataWeather != null ? SizedBox(
                height: 150,
                child: ListView.builder(
                  shrinkWrap: true,
                  scrollDirection: Axis.horizontal,
                  itemCount: ref.watch(controllerWeather).dataWeather!.forecast!.forecastday![0].hour!.length,
                  itemBuilder: ((context, index) {
                  return Padding(
                    padding: const EdgeInsets.only(left: 14.0, right: 10),
                    child: Column(
                      children: [
                        Image.network(
                          'https:${ref.watch(controllerWeather).dataWeather!.forecast!.forecastday![0].hour![index].condition!.icon!}',
                          width: 42,
                        ),
                        Text(
                          ref.watch(controllerWeather).dataWeather!.forecast!.forecastday![0].hour![index].condition!.text!,
                          style: GoogleFonts.montserrat(
                            color: masterColor('primary'),
                            fontSize: 10,
                            fontWeight: masterFontWeight('SB')
                          ),
                        ),
                        const SizedBox(height: 10),
                        Text(
                          convertDate(ref.watch(controllerWeather).dataWeather!.forecast!.forecastday![0].hour![index].time!),
                          style: GoogleFonts.montserrat(
                            color: masterColor('primary'),
                            fontSize: 12,
                            fontWeight: masterFontWeight('M')
                          ),
                        ),
                        const SizedBox(height: 10),
                        Row(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Text(
                              ref.watch(controllerWeather).dataWeather!.forecast!.forecastday![0].hour![index].tempC!.toString(),
                              style: GoogleFonts.montserrat(
                                color: masterColor('primary'),
                                fontSize: 18,
                                fontWeight: masterFontWeight('SB')
                              ),
                            ),
                            const SizedBox(width: 3),
                            Text(
                              "°",
                              style: GoogleFonts.montserrat(
                                color: masterColor('primary'),
                                fontSize: 12,
                                fontWeight: masterFontWeight('SB')
                              ),
                            ),
                          ],
                        )
                        
                      ],
                    ),
                  );
                })),
              ) : const SizedBox()
            ],
          ),
        ),
      ),
    );
  }
}