import 'package:cwaca_app/core/models/models.dart';
import 'package:cwaca_app/core/services/services.dart';
import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:geocoding/geocoding.dart';
import 'package:geolocator/geolocator.dart';

part 'controller_weather.dart';
