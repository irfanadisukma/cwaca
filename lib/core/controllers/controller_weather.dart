part of 'controllers.dart';

final controllerWeather = ChangeNotifierProvider<ControllerWeather>((ref) => ControllerWeather());

class ControllerWeather extends ChangeNotifier {
  WeatherModel? dataWeather;
  String currentLatLng = '';
  String? currentCity;
  String currentDistrict = '';

  double? latitude;
  double? longitude;

  Future<void> getLocation() async {
    LocationPermission permission;
    Position? currentPos;

    permission = await Geolocator.checkPermission();
    if ((permission != LocationPermission.always || permission != LocationPermission.whileInUse)) {
      permission = await Geolocator.requestPermission();
      currentPos = const Position(longitude: 0, latitude: 0, timestamp: null, accuracy: 0, altitude: 0, heading: 0, speed: 0, speedAccuracy: 0);
    }
    try {
      currentPos = await Geolocator.getCurrentPosition();
    } catch(e) {
      getLocation();
    }

    latitude = currentPos?.latitude;
    longitude  = currentPos?.longitude;
    List<Placemark> _placemark = await placemarkFromCoordinates(
      latitude!, longitude!,
      localeIdentifier: "id"
    );
    currentLatLng = "$latitude,$longitude";
    currentCity = _placemark.first.subLocality!;
    currentDistrict = _placemark.first.locality!;
    print("CURRENT CITY: $currentCity");
    print("CURRENT DISRTRICT: $currentDistrict");
    notifyListeners();
  }

  void getWeather() async {
    dataWeather = await WeatherService.getWeather(currentLatLng);
    
    notifyListeners();
  }

}