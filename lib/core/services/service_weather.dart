part of 'services.dart';

class WeatherService {

  static Future<WeatherModel> getWeather(String latLng) async {

    String url = "https://api.weatherapi.com/v1/forecast.json?key=$apikey&q=$latLng&days=1&aqi=no&alerts=no";

    var response = await http.get(
      Uri.parse(url),
      headers: {
        "HttpHeaders.contentTypeHeader": "application/json"
      },
    );
    print("responsenya: ${response.body}");
    final data = weatherModelFromJson(response.body);
    return data;
  }

}