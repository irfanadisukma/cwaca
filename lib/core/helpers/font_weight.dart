part of 'helpers.dart';

FontWeight masterFontWeight([String weight = 'N']) {
  late FontWeight returnedFW;
  switch (weight) {
    case 'T':
      returnedFW = FontWeight.w100;
      break;
    case 'XL':
      returnedFW = FontWeight.w200;
      break;
    case 'L': // Light
      returnedFW = FontWeight.w300;
      break;
    case 'N': // Normal
      returnedFW = FontWeight.w400;
      break;
    case 'M': // Medium
      returnedFW = FontWeight.w500;
      break;
    case 'SB': // Bold
      returnedFW = FontWeight.w600;
      break;
    case 'B':
      returnedFW = FontWeight.w700;
      break;
    case 'XB':
      returnedFW = FontWeight.w800;
      break;
    case 'TB':
      returnedFW = FontWeight.w900;
      break;
  }
  return returnedFW;
}