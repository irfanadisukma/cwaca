part of 'helpers.dart';

Color masterColor([String color = "default", double opacity = 1]) {
  Color returnedColor;
  color.toLowerCase();
  switch (color) {
    case "primary":
      returnedColor = const Color(0xFF354C89);
      break;
    case "primarydisabled":
      returnedColor = const Color(0xFF354C89).withOpacity(0.5);
      break;
    case "secondary":
      returnedColor = Colors.white;
      break;
    case "address":
      returnedColor = const Color(0xFF858585);
      break;
    case "semiblack":
      returnedColor = const Color(0xFF454545);
      break;
    case "divider":
      returnedColor = const Color(0xFFEDECEC);
      break;
    case 'discount':
      returnedColor = const Color(0xFFFD655D);
      break;
    case "danger":
      returnedColor = const Color(0xFFFF605D);
      break;
    case "apple":
      returnedColor = const Color(0xFF414042);
      break;
    case "facebook":
      returnedColor = const Color(0xFF4363A9);
      break;
    case "honey":
      returnedColor = const Color(0xFFFDDC96);
      break;
    case "chooseaddress":
      returnedColor = const Color(0xFFFFF4DD);
      break;
    case "selectedAddress":
      returnedColor = const Color(0xFFFAFAFF);
      break;
    case "grey":
      returnedColor = Colors.grey;
      break;
    case "darkgrey":
      returnedColor = const Color(0xFF9A9A9A);
      break;
    case "lightgrey":
      returnedColor = const Color(0xFFC4C4C4);
      break;
    case "disabled":
      returnedColor = const Color(0xFFF0F0F0);
      break;
    default:
      returnedColor = Color.fromRGBO(0, 0, 0, opacity);
      break;
  }
  return returnedColor;
}