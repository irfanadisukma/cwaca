import 'package:flutter/material.dart';
import 'package:intl/intl.dart';

part 'font_weight.dart';
part 'colors.dart';
part 'constant.dart';

 String convertDate(String time){
  DateTime parseDate = DateFormat("yyyy-MM-dd HH:mm").parse(time);
  var inputDate = DateTime.parse(parseDate.toString());
  var outputFormat = DateFormat('hh:mm a');
  var outputDate = outputFormat.format(inputDate);
  return outputDate;
}